require 'rails_helper'

RSpec.describe Url, :type => :model do
  it "should refuse blank url" do
		url = create(:url => "")
		expect(url).to eq false
	end

	it "should accept stringed url" do
		url = create(:url => "http://www.eruditescience.com")
		expect(url).to eq true
	end
		
end
