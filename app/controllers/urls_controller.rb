class UrlsController < ApplicationController
  def new
		@tiny_url = Url.new
  end

	def create
		@tiny_url = Url.new params[:url]
		if @tiny_url.save
			flash[:tiny_id] = @tiny_url.id
			redirect_to new_url_url
		else
			render :action => "new"
		end
	end

	def show
		@tiny_url = Url.find params[:id]
		redirect_to @tiny_url.url
	end
end
